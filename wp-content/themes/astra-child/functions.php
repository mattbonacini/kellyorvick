<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        wp_enqueue_style( 'chld_thm_cfg_child', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'astra-theme-css','astra-lifterlms','astra-contact-form-7','woocommerce-layout','woocommerce-smallscreen','woocommerce-general' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css', 10 );

// END ENQUEUE PARENT ACTION

function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), 1.3, 'all' );

}
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );


wp_enqueue_script('child_scripts',get_stylesheet_directory_uri().'/scripts.js',array('jquery' ) , 1.0 , true );


// Adding shortcode to show course description in sidebar/widgets areas
function information_template_parts() {
    ob_start();
    get_template_part( 'lifterlms/course/meta-wrapper-start' );
    get_template_part( 'lifterlms/course/difficulty' );
    get_template_part( 'lifterlms/course/length' );
    get_template_part( 'lifterlms/course/meta-wrapper-end' );
    return ob_get_clean();
}
add_shortcode('course_information_widget', 'information_template_parts');


// If the user is logged in make sure that he/she gets redirected to the log-in/register page
// this depends by the lifterLMS plugin
function inline_script_footer(){ ?>
<script>
(function($) {
  if ($('body.logged-in').length) {
    const log_out_link = '<?php echo wp_logout_url( llms_get_page_url( 'myaccount' ) ); ?>';
    // const log_out_link = "<?php // echo wp_logout_url('/my-courses'); ?>";
    const filtered_link = log_out_link.replace(/&amp;/g,'&');
    $('div[data-elementor-type="header"] .elementor-nav-menu--main .log_out_link_llms > a').attr('href', filtered_link);
  }
})(jQuery);
</script>
<?php }

add_action('wp_footer', 'inline_script_footer');



// Remove video related action in single lesson template. The video has been moved in a higher section of the template, you can find it in header.php:64
remove_action( 'lifterlms_single_lesson_before_summary', 'lifterlms_template_single_lesson_video', 20 );


function remove_dashboard_tabs_lifterLMS ( $tabs ) {
  unset( $tabs['my-grades'] );
  unset( $tabs['redeem-voucher'] );
  unset( $tabs['notifications'] );
	// unset( $tabs['view-courses'] );
	// unset( $tabs['view-achievements'] );
	// unset( $tabs['edit-account'] );
	// unset( $tabs['redeem-voucher'] );
	// unset( $tabs['orders'] );
	// unset( $tabs['signout'] );
	return $tabs;
}
add_filter( 'llms_get_student_dashboard_tabs', 'remove_dashboard_tabs_lifterLMS');







add_action( 'lifterlms_after_my_account_navigation', 'renaming_dashboard_tabs' );
function renaming_dashboard_tabs(){
  ?>
  <script type="text/javascript">
    (function($) {
    // Rename tabs in dashboard
    $('.llms-sd-nav .view-courses a').text('Courses');
    $('.llms-sd-nav .view-memberships a').text('Memberships');
    $('.llms-sd-nav .view-achievements a').text('Achievements');
    $('.llms-sd-nav .view-certificates a').text('Certificates');
    $('.llms-sd-nav .edit-account a').text('Edit Account');
    $('.llms-sd-nav .orders  a').text('Edit History');
  })(jQuery);
  </script>
  <?php
}
