<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<?php wp_head(); ?>
<?php astra_head_bottom(); ?>
</head>

<body <?php astra_schema_body(); ?> <?php body_class(); ?>>

<?php astra_body_top(); ?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>

	<?php astra_header_before(); ?>

	<?php astra_header(); ?>

	<?php astra_header_after(); ?>

	<?php astra_content_before(); ?>

<!-- Apply custom header if this is a single course -->
  <?php if (is_course()): ?>
    <?php

      $queried_object = get_queried_object();
      $course_title =  $queried_object->post_title;
      $course_feat_img_url = get_the_post_thumbnail_url($queried_object->ID, 'full');
      $course_desc = get_post_meta($queried_object->ID, "course-description-custom-header", true);

    ?>

    <div class="header_courses bg_cover" style="background-image: url(https://kellyorvick.com/wp-content/uploads/2019/01/people-woman-coffee-meeting-1024x683.jpg);">
      <div class="container">
        <h1 class="entry-title"><?php echo $course_title  ?></h1>
        <p class="archive-description"> <?php echo $course_desc ?> </p>
      </div>
    </div>



  <?php endif; ?>
	<div id="content" class="site-content">

	<?php
	if (is_lesson()) {
		llms_get_template( 'lesson/video.php' );
	}
	?>

		<div class="ast-container">
		<?php astra_content_top(); ?>
