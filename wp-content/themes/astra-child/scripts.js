(function($) {

// Custom edit to template for single course template & sidebar
  $('.llms-syllabus-wrapper .llms-lesson-counter').each(function(index) {
    const lesson_number = parseInt($(this).text());
    if (lesson_number < 10) {
      $(this).text('0' + lesson_number)
    }
  });

// Move site title in header to place it besides the title.
//Not using elementor in this one to avoid responsive design problems.
$('#website_title_text').appendTo('#logo_container .elementor-image > a');


// logo_container

// Code to keep the user on the same exact single course IF he/she goes towards LOG IN page
  const referrer =  document.referrer;
  if (referrer.indexOf('kellyorvick.com/course/') >= 0 && $('form.llms-login').length) {
    $('form.llms-login input[name="redirect"]').attr('value', referrer);
  }





})(jQuery);
